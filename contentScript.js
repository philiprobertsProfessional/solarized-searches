/*

Main content script for solarized extension
Loops through every element and changes it's color based on it's current color

$base03:    #002b36;
$base02:    #073642;
$base01:    #586e75;
$base00:    #657b83;
$base0:     #839496;
$base1:     #93a1a1;
$base2:     #eee8d5;
$base3:     #fdf6e3;
$yellow:    #b58900;
$orange:    #cb4b16;
$red:       #dc322f;
$magenta:   #d33682;
$violet:    #6c71c4;
$blue:      #268bd2;
$cyan:      #2aa198;
$green:     #859900;
*/

var Base03 = "'#002b36'";
var Base02 = "'#073642'";
var Base01 = "'#586e75'";
var Base00 = "'#657b83'";
var Base0 = "'#839496'";
var Base1 = "'#93a1a1'";
var Base2 = "'#eee8d5'";
var Base3 = "'#fdf6e3'";

window.onload = (event) => {
    //console.log('page is fully loaded');
    main();
};

function main(){

    styles = { 'backgroundColor': false, 'color': true, 'borderColor': false}

    for (const [key, value] of Object.entries(styles)) {
        changeColor(key, value);
    }

    

}

function changeSpecific(attribute, a){

    // Converts Color to Solarized

    var all = document.getElementsByTagName("*");

    for (var i=0, max=all.length; i < max; i++) {
        
        var rgb = (eval("(getComputedStyle(all[i])." + attribute + ")"));


        if(rgb == '#fff'){
            eval("all[i].style." + attribute + " = " + Base03 + ";");
        }else{
            console.log("HERE:" + rgb);
        }
        
    }
}

function changeColor(attribute, reverse){

    // Converts Color to Solarized + overloads method, adds option to reverse color scheme (meaning negative)

    var all = document.getElementsByTagName("*");

    if(reverse == false){
        for (var i=0, max=all.length; i < max; i++) {
            // Gets RGB for elements
            var rgbArray = (eval("(getComputedStyle(all[i])." + attribute + ")").match(/\d+/g));
    
            // Calulates Average Color for element
            try{
                rgbAvg = (parseInt(rgbArray[0]) + parseInt(rgbArray[1]) + parseInt(rgbArray[2])) / 3;
            }catch(e){
                rgbAvg = 255;
            }
    
            //console.log("rgbArray= " + rgbArray + " rgbAvg= " + rgbAvg);
    
            if(eval("(getComputedStyle(all[i])." + attribute + ") + ''")[4] == "("){
                eval("all[i].style." + attribute + " = " + Base03 + ";");
            }else if(rgbAvg < 32){
                eval("all[i].style." + attribute + " = " + Base02 + ";");
            }else if(rgbAvg < 64){
                eval("all[i].style." + attribute + " = " + Base01 + ";");
            }else if(rgbAvg < 96){
                eval("all[i].style." + attribute + " = " + Base00 + ";");
            }else if(rgbAvg < 116){
                eval("all[i].style." + attribute + " = " + Base0 + ";");
            }else if(rgbAvg < 136){
                eval("all[i].style." + attribute + " = " + Base1 + ";");
            }else if(rgbAvg < 156){
                eval("all[i].style." + attribute + " = " + Base2 + ";");
            }else if(rgbAvg < 226){
                eval("all[i].style." + attribute + " = " + Base1 + ";");
            }else{
                eval("all[i].style." + attribute + " = " + Base03 + ";");
            }
        }
    }else{
        for (var i=0, max=all.length; i < max; i++) {
            // Gets RGB for elements
            var rgbArray = (eval("(getComputedStyle(all[i])." + attribute + ") + ''").match(/\d+/g));
    
            // Calulates Average Color for element
            try{
                rgbAvg = (parseInt(rgbArray[0]) + parseInt(rgbArray[1]) + parseInt(rgbArray[2])) / 3;
            }catch(e){
                rgbAvg = 255;
            }
    
            //console.log("rgbArray= " + rgbArray + " rgbAvg= " + rgbAvg);
    
            if(eval("(getComputedStyle(all[i])." + attribute + ") + ''")[4] == "("){
                eval("all[i].style." + attribute + " = " + Base3 + ";");
            }else if(rgbAvg < 32){
                eval("all[i].style." + attribute + " = " + Base3 + ";");
            }else if(rgbAvg < 64){
                eval("all[i].style." + attribute + " = " + Base2 + ";");
            }else if(rgbAvg < 96){
                eval("all[i].style." + attribute + " = " + Base1 + ";");
            }else if(rgbAvg < 128){
                eval("all[i].style." + attribute + " = " + Base0 + ";");
            }else if(rgbAvg < 160){
                eval("all[i].style." + attribute + " = " + Base00 + ";");
            }else if(rgbAvg < 192){
                eval("all[i].style." + attribute + " = " + Base01 + ";");
            }else if(rgbAvg < 224){
                eval("all[i].style." + attribute + " = " + Base02 + ";");
            }else if(rgbAvg < 256){
                eval("all[i].style." + attribute + " = " + Base03 + ";");
            }else{
                eval("all[i].style." + attribute + " = " + Base03 + ";");
            }
        }
    }

    
}